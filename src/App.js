import React from "react";
import "./App.css";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import { Table, Container } from "reactstrap";
import BotonGroup from "./BotonGroup";

const url = "https://jsonfy.com/users";

class App extends React.Component {
  state = {
    data: [],
  };

  peticionGet = () => {
    axios.get(url).then((response) => {
      //  debugger;
      this.setState({ data: response.data });
    });
  };
  componentDidMount() {
    this.peticionGet();
  }

  render() {
    return (
      <div>
        <Container >
          <BotonGroup></BotonGroup>
          <hr />
          <Table striped>
            <thead>
              <tr>
                <th scope="row">Id</th>
                <th>Nombre</th>
                <th>Username</th>
                <th>Email</th>
                <th>Password</th>
                <th>Age</th>
                <th>Website</th>
                <th>Phone</th>
                <th>Date_Add</th>
                <th>Password_md5</th>
              </tr>
            </thead>

            <tbody>
              {this.state.data.map((usuario) => {
                return (
                  <tr>
                    <td>{usuario.id}</td>
                    <td>{usuario.name}</td>
                    <td>{usuario.username}</td>
                    <td>{usuario.email}</td>
                    <td>{usuario.password}</td>
                    <td>{usuario.age}</td>
                    <td>{usuario.website}</td>
                    <td>{usuario.phone}</td>
                    <td>{usuario.date_add}</td>
                    <td>{usuario.date_upd}</td>
                    <td>{usuario.password_md5}</td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        </Container>
      </div>
    );
  }
}

export default App;
