import React from 'react';
import { Button, ButtonGroup } from 'reactstrap';

const BotonGroup = (props) => {
  return (
    <ButtonGroup>
      <Button>15</Button>
      <Button>30</Button>
      <Button>45</Button>
    </ButtonGroup>
  );
}

export default BotonGroup;